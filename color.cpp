#include "color.h"

color::color(int r, int g, int b) :
	r(r), g(g), b(b)
{
}

const color color::red = color(255, 0, 0);
