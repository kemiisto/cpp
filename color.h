#ifndef COLOR_H
#define COLOR_H

struct color
{
	color(int r, int g, int b);
	static const color red;
	int r, g, b;
};

#endif // COLOR_H
