#ifndef MATERIAL_H
#define MATERIAL_H

#include "color.h"

struct material
{
	material(color c);
	static const material red;
	color c;
};

#endif // MATERIAL_H
